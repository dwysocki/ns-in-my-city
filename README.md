# Neutron star in my city

Most things in space may seem unfathomably large.  Neutron stars, which are as massive as our sun, are one exception, having roughly the diameter of a large city.  Similarly massive black holes are even smaller.  With this app, you can go to any location on Earth, and see how big a neutron star or black hole would be in comparison.

[Check it out here!](https://neutron-star.org)


## Testing locally

While the production deployment runs on Google Kubernetes Engine, you can test it out using a local Kubernetes cluster of your choosing.  Here are the directions to use [MiniKube](https://minikube.sigs.k8s.io/docs/) specifically.

```bash
# Run minikube with Docker as the container runtime
minikube start --container-runtime=docker

# Provide your shell with necessary env variables
eval $(minikube -p minikube docker-env)

# Build the server container
(cd server; docker build . -t local/ns-in-my-city/server:latest)

# Build the location service container
(cd location-service; docker build . -t local/ns-in-my-city/location:latest)

# Build the QR service container
(cd qr-service; docker build . -t local/ns-in-my-city/qr:latest)

# Deploy the Kubernetes cluster
minikube kubectl -- apply -f manifests/ns-in-my-city:local.yaml

# View the app in a browser by clicking or copy+pasting the URL this produces:
minikube service nimc-server-svc --url

# When you're done, you probably want to stop minikube
minikube stop
```


## Authors and acknowledgment

This project was created by Daniel Wysocki and HanGyeol Suh.

It was created using [CesiumJS](https://cesium.com/platform/cesiumjs/).


## License

MIT License

Copyright (c) 2022 Daniel Wysocki and HanGyeol Suh
