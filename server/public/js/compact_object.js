// abstract compact object entitites
class CompactObject {
    constructor(name, mdefault, mmin, mmax, radii) {
        if (this.constructor == CompactObject) {
            throw new Error("Abstract classes can't be instantiated.");
        }

        this.name = name;

        this.mdefault = mdefault;
        this.mmin = mmin;
        this.mmax = mmax;
        this.radii = radii;

        this.massRange = mmax - mmin;
        this.dm = this.massRange / (this.radii.length - 1);
    }

    // gives the radius for a chosen mass
    radius(mass) {
        // Get the closest index for the given mass.
        const i = Math.floor((mass - this.mmin) / this.dm);
        // Return the corresponding tabulated radius.
        return this.radii[i];
    }

    // signals that the compact object is collapsing to a black hole
    isCollapsing(mass) {
        throw new Error("Method 'isCollapsing' must be implemented.");
    }

    isValidMass(mass) {
        return (this.mmin <= mass) && (mass <= this.mmax);
    }

    massFromSlider(slider) {
        return (
            this.mmin +
            this.massRange * slider.value / CONFIG.SLIDER_POINTS
        );
    }

    massToSliderValue(mass) {
        return Math.floor(
            (mass - this.mmin) *
            CONFIG.SLIDER_POINTS / this.massRange
        );
    }
}

// Fiducial M(R) taken from http://xtreme.as.arizona.edu/NeutronStars/
// using SLy model.

const neutronStarFiducialMMin = 0.212;
const neutronStarFiducialMMax = 2.025;

const neutronStarFiducialRadii = [
    15.806, 15.643, 15.491, 15.347, 15.212, 15.085, 14.965, 14.852,
    14.745, 14.643, 14.547, 14.456, 14.369, 14.277, 14.148, 14.028,
    13.916, 13.811, 13.713, 13.622, 13.536, 13.457, 13.382, 13.313,
    13.247, 13.186, 13.129, 13.075, 13.024, 12.976, 12.932, 12.889,
    12.849, 12.811, 12.775, 12.741, 12.707, 12.663, 12.621, 12.582,
    12.546, 12.512, 12.480, 12.451, 12.424, 12.398, 12.374, 12.352,
    12.331, 12.311, 12.293, 12.276, 12.260, 12.244, 12.230, 12.216,
    12.198, 12.182, 12.167, 12.152, 12.139, 12.127, 12.116, 12.105,
    12.095, 12.086, 12.077, 12.069, 12.061, 12.054, 12.047, 12.040,
    12.033, 12.025, 12.018, 12.011, 12.005, 11.999, 11.993, 11.987,
    11.982, 11.977, 11.972, 11.968, 11.963, 11.958, 11.954, 11.948,
    11.943, 11.938, 11.933, 11.928, 11.923, 11.918, 11.913, 11.908,
    11.904, 11.899, 11.894, 11.889, 11.883, 11.877, 11.871, 11.865,
    11.859, 11.853, 11.846, 11.840, 11.834, 11.827, 11.821, 11.814,
    11.806, 11.798, 11.790, 11.782, 11.774, 11.766, 11.758, 11.749,
    11.741, 11.732, 11.723, 11.713, 11.703, 11.693, 11.683, 11.672,
    11.662, 11.652, 11.641, 11.630, 11.620, 11.607, 11.595, 11.583,
    11.570, 11.558, 11.545, 11.533, 11.520, 11.508, 11.494, 11.480,
    11.466, 11.452, 11.437, 11.423, 11.409, 11.394, 11.380, 11.365,
    11.349, 11.334, 11.318, 11.302, 11.286, 11.270, 11.254, 11.239,
    11.222, 11.205, 11.188, 11.171, 11.154, 11.137, 11.120, 11.103,
    11.086, 11.067, 11.049, 11.031, 11.013, 10.995, 10.978, 10.960,
    10.942, 10.923, 10.904, 10.886, 10.867, 10.848, 10.830, 10.811,
    10.793, 10.773, 10.754, 10.735, 10.716, 10.697, 10.678, 10.660,
    10.640, 10.621, 10.601, 10.582, 10.563, 10.544, 10.525, 10.505,
    10.486, 10.466, 10.447, 10.427, 10.408, 10.389, 10.370, 10.350,
    10.331, 10.312, 10.292, 10.273, 10.255, 10.235, 10.216, 10.196,
    10.177, 10.158, 10.140, 10.121, 10.102, 10.083, 10.064, 10.045,
    10.026, 10.008, 9.989,
].map(radius => radius * 1e3);

class NeutronStar extends CompactObject {
    constructor() {
        super(
            "Neutron Star",
            1.4,
            neutronStarFiducialMMin, neutronStarFiducialMMax,
            neutronStarFiducialRadii,
        );
    }

    // determines whether NS has reached maximum mass
    isCollapsing(mass) {
        // todo: use maximum mass from a real M(R) relation
        return mass >= this.mmax;
    }
}
const neutronStar = new NeutronStar()


const blackHoleMasses = Array.from(
    new Array(CONFIG.SLIDER_POINTS),
    (x, i) => {
        const MASS_RANGE = CONFIG.MAX_MASS - CONFIG.MIN_MASS;
        return CONFIG.MIN_MASS + i * MASS_RANGE / (CONFIG.SLIDER_POINTS-1);
    },
);
const blackHoleRadii = blackHoleMasses.map(mass => {
    return PHYS.SCHWARZSCHILD * mass;
});

class BlackHole extends CompactObject {
    constructor() {
        super(
            "Black Hole",
            1.0,
            blackHoleMasses[0],
            blackHoleMasses[blackHoleMasses.length - 1],
            blackHoleRadii,
        );
    }

    // black holes already collapsed
    isCollapsing(mass) {
        return false;
    }
}
const blackHole = new BlackHole();
