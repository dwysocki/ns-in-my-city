// generalized compact object geometry, can swap between NS and BH
class COGeometry {
    constructor(viewer, initGroundLocation) {
        this.groundLocation = initGroundLocation;

        this.models = this.initModels();
        this.addEntitiesToViewer(viewer);
        this.setInitialModelAndCompactObject();

        this.mass = this.initMass();
        this.radius = this.compactObject.radius(this.mass);

        this.showInitialModel();
        this.shadow = this.initShadow(viewer);
    }

    initMass() {
        // Try to set initial mass from the URL query
        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString);

        if (urlParams.has("mass")) {
            const paramMass = parseFloat(urlParams.get("mass"));

            if (isNaN(paramMass)) {
                console.log("Mass was not a valid number");
                return this.compactObject.mdefault;
            } else if (!this.compactObject.isValidMass(paramMass)) {
                console.log("Mass was outside the object's valid range");
                return this.compactObject.mdefault;
            } else {
                return paramMass;
            }
        } else {
            // No mass in URL
            return this.compactObject.mdefault;
        }
    }

    initModels() {
        const NS = createModel("models/neutron_star.glb");
        const BH = createModel("models/black_hole.glb");

        return {
            NS: NS,
            BH: BH,
        };
    }

    setInitialModelAndCompactObject() {
        // Try to set initial model from the URL query
        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString);

        if (!urlParams.has("type")) {
            this.entity = this.models.NS;
            this.compactObject = neutronStar;
            return;
        }

        switch (urlParams.get("type").toLowerCase()) {
        case "neutron star":
            this.entity = this.models.NS;
            this.compactObject = neutronStar;
            break;
        case "black hole":
            this.entity = this.models.BH;
            this.compactObject = blackHole;
            break;
        default:
            console.log("Unknown object type in URL");
            this.entity = this.models.NS;
            this.compactObject = neutronStar;
            break;
        }
    }

    showInitialModel() {
        this.entity.position = this.position;
        this.entity.model.scale = this.radius;
        this.entity.show = true;
    }

    initShadow(viewer) {
        // NOTE: creating the `Cesium.Entity` first causes a crash which seems
        // to be related to a version mismatch for Handlebars.  Odd because we
        // do not do any dependency management outside of loading CesiumJS.
        return viewer.entities.add({
            position: Cesium.Cartesian3.fromRadians(
                this.groundLocation.longitude, this.groundLocation.latitude, 0,
            ),
            ellipse: {
                semiMinorAxis: this.radius,
                semiMajorAxis: this.radius,
//                material: Cesium.ColorMaterialProperty(Cesium.Color.BLACK.withAlpha(0.5)), // shows up white for some reason
                // workaround: checkerboard seems to work while using the color directly does not.
                material : new Cesium.CheckerboardMaterialProperty({
                    evenColor : Cesium.Color.BLACK.withAlpha(0.5),
                    oddColor : Cesium.Color.BLACK.withAlpha(0.5),
                    repeat : new Cesium.Cartesian2(1, 1)
                })
            },
        });
    }

    addEntitiesToViewer(viewer) {
        for (const [key, val] of Object.entries(this.models)) {
            viewer.entities.add(val);
        }
//        viewer.entities.add(this.shadow);
    }

    get sliderValue() {
        return this.compactObject.massToSliderValue(this.mass);
    }

    get position() {
        return Cesium.Cartesian3.fromRadians(
            this.groundLocation.longitude, this.groundLocation.latitude,
            CONFIG.COMPACT_OBJECT_HEIGHT_OFFSET + this.radius,
        );
    }

    moveTo(groundLocation) {
        this.groundLocation = groundLocation;
        this.entity.position = this.position;
        this.shadow.position = Cesium.Cartesian3.fromRadians(
            this.groundLocation.longitude, this.groundLocation.latitude, 0,
        );
    }

    updateMass() {
        // update mass value
        const slider = document.getElementById("massSlider");
        this.mass = this.compactObject.massFromSlider(slider);

        // potentially collapse to a black hole
        const isCollapsing = this.compactObject.isCollapsing(this.mass)
        if (isCollapsing) {
            this.compactObject = blackHole;
            this.entity = this.models.BH;
        }

        // update radius
        this.radius = this.compactObject.radius(this.mass);
        this.updateEntityRadius();

        // change shown entity if collapsed to a black hole
        if (isCollapsing) {
            this.models.BH.show = true;
            this.models.NS.show = false;
            this.resetMassSlider();
            this.updateTypePicker();
        }
    }

    updateEntityRadius() {
        this.entity.model.scale = this.radius;
        this.entity.position = this.position;
        this.shadow.ellipse.semiMinorAxis = this.radius;
        this.shadow.ellipse.semiMajorAxis = this.radius;
    }

    updateType() {
        const slider = document.getElementById("massSlider");
        const select = document.getElementById("typePicker");
        const newType = select.value;

        if (newType == "Neutron Star") {
            this.compactObject = neutronStar;
            this.entity = this.models.NS;

            this.mass = this.compactObject.mmin;
            this.radius = this.compactObject.radius(this.mass);
            this.updateEntityRadius();

            this.models.NS.show = true;
            this.models.BH.show = false;
        } else if (newType == "Black Hole") {
            this.compactObject = blackHole;
            this.entity = this.models.BH;

            this.mass = this.compactObject.mmin;
            this.radius = this.compactObject.radius(this.mass);
            this.updateEntityRadius();

            this.models.BH.show = true;
            this.models.NS.show = false;
        } else {
            console.log(`Unknown compact object type: ${newType}`);
        }

        this.resetMassSlider();
    }
}


function createModel(url) {
  return new Cesium.Entity({
      name: url,
      model: {
          uri: url,
          shadows: Cesium.ShadowMode.DISABLED,
      },
      show: false,
  });
}
