class MassSlider {
    constructor() {
        this.input = this.createInput();
        this.div = document.getElementById("massSliderContainer");
        this.div.appendChild(this.input);
        this.callbacks = [];
    }

    createInput(initValue = 0) {
        const elem = document.createElement("input");
        elem.setAttribute("type", "range");
        elem.setAttribute("min", 0);
        elem.setAttribute("max", CONFIG.SLIDER_POINTS);
        elem.setAttribute("value", initValue);
        elem.className = "slider";
        elem.id = "massSlider";
        return elem;
    }

    resetInput(value) {
        // Create new slider.
        const newInput = this.createInput(value);
        // Delete old slider from div.
        this.div.removeChild(this.input);
        // Add new slider to div.
        this.div.appendChild(newInput);
        // Add new slider to class
        this.input = newInput;
        // Reapply listeners to new slider
        this.reapplyInputListeners();
    }

    addInputListener(callback) {
        this.callbacks.push(callback);
        this.input.addEventListener("input", callback);
    }

    reapplyInputListeners() {
        for (const callback of this.callbacks) {
            this.input.addEventListener("input", callback);
        }
    }
}

class MassDisplay {
    constructor(compactObject) {
        this.compactObject = compactObject;

        this.div = document.getElementById("massDisplayContainer");
        this.div.innerHTML = this.text;
    }

    get slider() {
        return document.getElementById("massSlider");
    }

    get text() {
        const mass = this.compactObject.compactObject.massFromSlider(this.slider);
        return `${mass.toFixed(1)} &times; the Sun's mass`;
    }

    update() {
        this.div.innerHTML = this.text;
    }
}

class TypePicker {
    constructor() {
        this.select = document.getElementById("typePicker");
        this.addOption("Neutron Star");
        this.addOption("Black Hole");
    }

    addOption(name) {
        const index = this.select.options.length;
        const option = document.createElement("option");
        option.text = name;

        this.select.options.add(option, index);
    }


    addChangeListener(callback) {
        this.select.addEventListener("change", callback);
    }

    updateElement(value) {
        this.select.value = value;
    }
}

function setCollapse() {
    const toolbarHeader = document.getElementById("toolbar-table").tHead;
    const isCollapsed = toolbarHeader.classList.contains("collapsed");

    // Get the chevron
    const toolbarChevron = document.getElementById("toolbar-chevron");
    // Get the toolbar body
    const toolbarBody = document.getElementById("toolbar-table").tBodies[0];
    if (isCollapsed) {
        // We're collapsing

        // Display the collapsed chevron
        toolbarChevron.innerHTML = "\uf054";
        // Hide the toolbar body
        toolbarBody.style.display = "none";
    } else {
        // We're expanding

        // Display the expanded chevron
        toolbarChevron.innerHTML = "\uf078";
        // Show the toolbar body
        toolbarBody.style.display = "block";
    }
}

function initToolbar() {
    const toolbarHeader = document.getElementById("toolbar-table").tHead;

    // Set all elements to be consistently collapsed or expanded
    setCollapse();

    // Add collapse/expand toggle functionality on toolbar header
    toolbarHeader.addEventListener("click", () => {
        const isCollapsed = toolbarHeader.classList.contains("collapsed");

        if (isCollapsed) {
            // Remove collapsed class from header
            toolbarHeader.classList.remove("collapsed");
        } else {
            // Add collapsed class to header
            toolbarHeader.classList.add("collapsed");
        }

        // Set all elements to be consistently collapsed or expanded
        setCollapse();
    });
}

function initShareButton(compactObject) {
    const toolbar = document.querySelector("div.cesium-viewer-toolbar");
    const helpButton = document.querySelector("span.cesium-navigation-help-button");
    const shareButton = document.createElement("button");
    shareButton.classList.add("cesium-button", "cesium-toolbar-button");
    shareButton.innerHTML = '<i class="fa-solid fa-share-nodes" id="share-button" title="Share"></i>';
    toolbar.insertBefore(shareButton, helpButton);
    const shareOverlayToggle = toggleOverlay("share")
    shareButton.addEventListener("click", () => {
        // Update URL in share overlay
        shareOverlayUpdate(compactObject);
        // Toggle visibility of the share overlay.
        shareOverlayToggle();
    });
}


function initAboutButton() {
    const toolbar = document.querySelector("div.cesium-viewer-toolbar");
    const helpButton = document.querySelector("span.cesium-navigation-help-button");
    const aboutButton = document.createElement("button");
    aboutButton.classList.add("cesium-button", "cesium-toolbar-button");
    aboutButton.innerHTML = '<i class="fa-solid fa-circle-info" id="info-button" title="About this app"></i>';
    toolbar.insertBefore(aboutButton, helpButton);
    aboutButton.addEventListener("click", toggleOverlay("about"));
}

function setOverlayVisibility(overlay, isHidden) {
    if (isHidden) {
        overlay.style.display = "block";
    } else {
        overlay.style.display = "none";
    }
}

function toggleOverlay(overlayId) {
    const overlay = document.getElementById(overlayId);
    return function() {
        const isHidden = overlay.classList.contains("hidden");
        setOverlayVisibility(overlay, isHidden);
        if (isHidden) {
            // Hide all other overlays.
            hideOtherOverlays(overlayId);
            // Show this overlay.
            overlay.classList.remove("hidden");
        } else {
            // Hide this overlay.
            overlay.classList.add("hidden");
        }
    }
}

function hideOtherOverlays(overlayId) {
    // Get all overlays
    const overlays = document.getElementsByClassName("overlay");

    // Hide all of the overlays that don't match the given ID
    for (var i = 0; i < overlays.length; i++) {
        const overlay = overlays[i];
        if (overlay.id != overlayId) {
            overlay.classList.add("hidden");
            overlay.style.display = "none";
        }
    }
}

function shareOverlayUpdate(compactObject) {
    const shareURL = getShareURL(compactObject);
    const shareQRImg = document.getElementById("share-qr-img");

    // Request the QR code, and update the QR image when it's received
    fetch("/qr", {method: "POST", body: shareURL})
      .then(response => {
        if (response.status === 200) {
            return response.blob();
        } else {
            throw Error(response.statusText);
        }
    }).then(imageBlob => {
        const imageObjectURL = URL.createObjectURL(imageBlob);
        shareQRImg.src = imageObjectURL;
    }).catch(error => {
        console.log(error);
    });

    // Update the displayed URL
    const shareURLInput = document.getElementById("share-url-input");
    shareURLInput.value = shareURL;
}

function getShareURL(compactObject) {
    const url = new URL(window.location.href);
    url.searchParams.set("type", compactObject.compactObject.name);
    url.searchParams.set("mass", compactObject.mass);
    url.searchParams.set(
        "longitude",
        Cesium.Math.toDegrees(compactObject.groundLocation.longitude),
    );
    url.searchParams.set(
        "latitude",
        Cesium.Math.toDegrees(compactObject.groundLocation.latitude),
    );

    return url;
}

function copyShareURL() {
    const shareURLInput = document.getElementById("share-url-input");
    navigator.clipboard.writeText(shareURLInput.value);
    // TODO: add floating indicator text
}
