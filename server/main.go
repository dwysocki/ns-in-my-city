package main

import (
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"

	//	"github.com/gin-contrib/static"
	"github.com/gin-gonic/gin"
)

func main() {
	router := gin.Default()

	// Don't trust any proxies
	router.SetTrustedProxies(nil)

	// Health check endpoint
	router.GET("/healthz", healthz)

	// Reverse-proxy /location
	router.GET("/location", ReverseProxy("http://nimc-location-svc:80"))
	// Reverse-proxy /qr
	router.POST("/qr", ReverseProxy("http://nimc-qr-svc:80"))

	// Route static frontend resources
	router.Static("/favicon.ico", "/public/favicon.ico")
	router.Static("/css", "/public/css")
	router.Static("/js", "/public/js")
	router.Static("/img", "/public/img")
	router.Static("/models", "/public/models")

	// Route index
	indexData, err := ioutil.ReadFile("/public/index.html")
	if err != nil {
		log.Fatal(err)
	}
	router.GET("/", func(c *gin.Context) {
		c.Data(http.StatusOK, "text/html", indexData)
	})
	//	router.Static("/index.htm", "/public/index.htm")

	if err := router.Run(":80"); err != nil {
		log.Fatal(err)
	}
}

// Adapted from:
// https://le-gall.bzh/post/go/a-reverse-proxy-in-go-using-gin/
func ReverseProxy(target string) gin.HandlerFunc {
	remote, err := url.Parse(target)
	if err != nil {
		panic(err)
	}

	return func(c *gin.Context) {
		log.Printf(
			"Reverse proxying to: %s\nClient IP: %s\nForward IPs: %s\n",
			target,
			c.ClientIP(),
			c.Request.Header.Get("X-Forwarded-For"),
		)

		proxy := httputil.NewSingleHostReverseProxy(remote)
		// Define the director func
		proxy.Director = func(req *http.Request) {
			req.Header = c.Request.Header
			req.Host = remote.Host
			req.URL.Scheme = remote.Scheme
			req.URL.Host = remote.Host
			req.URL.Path = c.Param("proxyPath")
		}

		proxy.ServeHTTP(c.Writer, c.Request)
	}

}

func healthz(c *gin.Context) {
	c.String(http.StatusOK, "Success")
}
