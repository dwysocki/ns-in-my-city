package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/hashicorp/golang-lru"
)

// Struct holding location information
type Location struct {
	Latitude  string
	Longitude string
}

// HTTP client
var client = &http.Client{Timeout: 5 * time.Second}

// Cache mapping IP Address -> Location
var loc_cache *lru.Cache

// API key
var API_KEY string

// Set to environment variable specifying that client IP address should be set
// to a fixed value for local testing purposes.
var USE_FIXED_IP bool

// IP address owned by the City of Milwaukee
const FIXED_IP = "199.196.86.137"

func main() {
	// Set up the cache of GPS coordinates
	loc_cache, _ = lru.New(128)
	// Get the API key from the env variable
	API_KEY = os.Getenv("IP_GEO_LOC_API_KEY")
	// Set whether to use a fixed client IP address
	setUseFixedIP()

	router := gin.Default()

	// Don't trust any proxies
	router.SetTrustedProxies(nil)

	// Health check endpoint
	router.GET("/healthz", healthz)

	router.GET("/", getLocation)

	router.Run(":80")
}

func getLocation(c *gin.Context) {
	var loc *Location
	var status int
	var ipAddress string

	// Get the IP address from the request
	if USE_FIXED_IP {
		ipAddress = FIXED_IP
	} else {
		ipAddress = getIPAddress(c)
	}

	if obj, ok := loc_cache.Get(ipAddress); ok {
		loc = obj.(*Location)
		// Return cached location
		c.JSON(http.StatusOK, loc)
	} else {
		// Request GPS information from ipgeolocation.io
		url := fmt.Sprintf(
			"https://api.ipgeolocation.io/ipgeo?apiKey=%s&ip=%s",
			API_KEY, ipAddress,
		)
		log.Printf("Requesting location from: %s\n", url)
		resp, _ := client.Get(url)
		// Handle errors
		// TODO: Handle individual error types gracefully
		if resp.StatusCode != http.StatusOK {
			c.String(
				http.StatusBadRequest,
				"Error communicating with IPGeoLocation, status: %d",
				resp.StatusCode,
			)
			return
		}

		// No HTTP errors, parse the attached JSON
		status, loc = parseLocation(resp)
		resp.Body.Close()

		// Cache the location
		loc_cache.Add(ipAddress, loc)

		// Respond with the location as JSON
		c.JSON(status, loc)
	}
}

func getIPAddress(c *gin.Context) (ipAddress string) {
	// Will use the client's IP address if not forwarded
	ipAddress = c.ClientIP()
	log.Printf("Client IP: %s\n", ipAddress)

	// Get the forwarding address(es) from the request header
	fwdAddresses := c.Request.Header.Get("X-Forwarded-For")
	log.Printf("Client IP: %s\nForward IPs: %s\n", ipAddress, fwdAddresses)
	if fwdAddresses != "" {
		// Possibly have multiple IP addresses from each proxy,
		// take the first IP in the list
		ipAddress = strings.Split(fwdAddresses, ", ")[0]
	}

	return
}

func parseLocation(r *http.Response) (status int, loc *Location) {
	// Try to decode the request body as Location, return a 400 status code
	// if failed.
	loc = new(Location)
	err := json.NewDecoder(r.Body).Decode(&loc)
	if err != nil {
		status = http.StatusBadRequest
		return
	}
	log.Printf("Parsed loc: %s\n", loc)

	// Decoded successfully, return a 200 status code.
	status = http.StatusOK
	return
}

func setUseFixedIP() {
	valStr, isSet := os.LookupEnv("USE_FIXED_IP")

	if isSet {
		// Parse boolean
		val, err := strconv.ParseBool(valStr)
		if err != nil {
			log.Printf("Error parsing env var USE_FIXED_IP: %s\n", err.Error)
		}
		// Set parameter to parsed value
		USE_FIXED_IP = val
	}
}

func healthz(c *gin.Context) {
	c.String(http.StatusOK, "Success")
}
