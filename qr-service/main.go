package main

import (
	"github.com/gin-gonic/gin"
	qrcode "github.com/skip2/go-qrcode"

	"io"
	"log"
	"net/http"
	"strings"
)

const QRErrRecoveryLevel = qrcode.Medium
const QRImageSize = 256

func main() {
	router := gin.Default()

	// Don't trust any proxies
	router.SetTrustedProxies(nil)

	// Health check endpoint
	router.GET("/healthz", healthz)
	// QR code endpoint
	router.POST("/", getQRCode)

	router.Run(":80")
}

func getQRCode(c *gin.Context) {
	// Get the URL to encode
	req := c.Request
	body := req.Body
	urlBuilder := new(strings.Builder)
	_, err := io.Copy(urlBuilder, body)
	if err != nil {
		log.Print(err)
		c.Status(http.StatusInternalServerError)
		return
	}
	url := urlBuilder.String()

	// Encode to a PNG
	png, err := qrcode.Encode(url, QRErrRecoveryLevel, QRImageSize)
	if err != nil {
		log.Print(err)
		c.String(
			http.StatusBadRequest,
			"The URL you tried to encode could not be processed: %s",
			url,
		)
		return
	}

	// Respond 200 OK with the QR code binary in the body
	c.Data(http.StatusOK, "image/png", png)
}

func healthz(c *gin.Context) {
	c.String(http.StatusOK, "Success")
}
